# # Build stage
# FROM node:latest as build

# # Create a new directory for the app
# RUN mkdir -p /app

# # Set the new directory as the working directory
# WORKDIR /app

# # Copy the package.json and package-lock.json files to the new directory
# COPY package*.json ./

# # Install the app's dependencies
# RUN npm install

# # Copy the rest of the app's files to the new directory
# COPY . .

# # Build the app
# RUN npm run build  --prod
# RUN ls /app/dist/

# # Serve stage
# FROM nginx:latest

# # Copy the built app files from the build stage to the nginx image
# COPY --from=build /app/dist/demo-app /usr/share/nginx/html

# # Expose port 80 to allow connections to the app
# EXPOSE 80

# # Start nginx 
# #CMD ["nginx", "-g", "daemon off;"]

FROM nginx:latest

# Copy the built app files from the build stage to the nginx image
COPY dist/demo-app /usr/share/nginx/html

# Expose port 80 to allow connections to the app
EXPOSE 80
